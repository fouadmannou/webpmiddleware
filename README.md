# Webp  Middlware
Django middleware that converts contentful images in response html to webp format whenever the client supports it.

## How to install

### Manual 

Copy middleware file content into a file in your project app, make sure to change middleware string in MIDDLEWARE variable to :

```'[APP_NAME].wepmiddleware.WebpMiddleware'```

### Automatic

Make sure your machine user has access to this private repo before you process with automatic approach.

Add new line to requirements file
```
git+https://git@bitbucket.org/funkycorp/webpmiddleware.git
```

### Usage

Add the middleware to your project settings :

```
MIDDLEWARE = [
    ...
    'wepmiddleware.WebpMiddleware'
    ...
]
```


