from setuptools import setup, find_packages

setup(name='webpmiddleware',
      version='0.1',
      description='Convert Contentful images in django respose to webp format.',
      url='git@bitbucket.org:fouadmannou/webpmiddleware.git',
      author='Fouad Mannou',
      author_email='fouad@funlycorp.com',
      license='unlicense',
      packages=["webpmiddleware"],
      zip_safe=False)