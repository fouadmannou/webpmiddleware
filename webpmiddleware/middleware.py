import re
class WebpMiddleware(object):
    def process_response(self, request, response):
        if "text/html" in request.META["HTTP_ACCEPT"] and "image/webp" in request.META["HTTP_ACCEPT"]:
            try:
              return self.apply_webp_transformation(response)
            except:
              pass
        return response

    def apply_webp_transformation(self, response):
        for bg in re.findall('(?:(?:https?|http):\/\/)?[\w]+\.ctfassets\.[\w/\-?=%.&]+', str(response.content)):
            if bg.find('fm')<0:
              if bg.find('?') >= 0:
                response.content = response.content.decode('utf-8').replace(bg, bg + "&fm=webp&q=70")
              else:
                response.content = response.content.decode('utf-8').replace(bg, bg + "?fm=webp&q=70")
        return response